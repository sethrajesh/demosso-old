<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>EResponse SSO Login Page</title>
    <!--  for IE8 support -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <link href="css/login.css" rel="stylesheet" type="text/css" media="all"/>
    <!--[if lt IE 7]>
    <![endif]-->
<style type="text/css">
.bgimg {
    background-image: url(images/page3.png);
}
input[type=submit] {
    background: url(images/SIDES_EResponse_Page2-2-1.png) no-repeat;
    border: 0;
    display: block;
    height: 24px;
    width: 137px;
    vertical-align: middle;
}

</style>
</head>

<body bgcolor="#ffffff" >
  
  <!-- Begin Header -->
  <div style="background-image:url(images/utah2-27.png);padding:5px;width:1300px;height:1150px;">
  
  <form name='f' action="<c:url value='j_spring_security_check' />" method="POST">
	<center>
		<table align="center" border="0" width="240">
			<tr>
					<td>&nbsp;
				</td>
			</tr>
			<tr>
					<td>&nbsp;
				</td>
			</tr>
			<tr>
					<td>&nbsp;
				</td>
			</tr>
			<tr>
					<td>&nbsp;
				</td>
			</tr>
			<tr>
					<td>&nbsp;
				</td>
			</tr>
			<tr>
					<td>&nbsp;
				</td>
			</tr>
			<tr>
					<td>&nbsp;
				</td>
			</tr>
			<tr>
					<td>&nbsp;
				</td>
			</tr>
			<tr>
					<td>&nbsp;
				</td>
			</tr>
			<tr>
					<td>&nbsp;
				</td>
			</tr>
			<tr>
					<td>&nbsp;
				</td>
			</tr>
			<tr>

					<td>&nbsp;
				</td>
			</tr>
<c:if test="${fn:contains(header['User-Agent'],'Chrome')}">
                        <tr>
                                        <td>&nbsp;
                                </td>
                        </tr>
                        <tr>
                                        <td>&nbsp;
                                </td>
                        </tr>

</c:if>
			<tr>
				<td>&nbsp;</a>
				</td>
			</tr>
						<tr>

                                        <td>&nbsp;
                                </td>
                        </tr>
                        <tr>

                                        <td align="left">	&nbsp;				 
                                </td>
                        </tr>
			
			<tr>
					<td align="left">
						<a target="_blank" href="<c:url value="/encrypttocas/${username}" />"><img src="images/trans.gif" alt="SIDES Logo" width="200"/></a>
			 	    </td>
			</tr>
			</tr>
			<tr>
				<td>&nbsp;</a>
				</td>
			</tr>
			<tr>
				<td align="left"><a target="_blank" href="<c:url value="/encrypttocas/new/${username}" />"><img src="images/trans.gif" alt="SIDES Logo" width="200" height="40"/></a>
				</td>
			</tr>
			<tr>
              <td align="left">
              	&nbsp;
              </td>
            </tr>	
			<tr>
					<td>
						&nbsp;
			 	    </td>
			</tr>
		</table>
	</center>
	</form>
    </div>
  </div>
  
  </div>
  
 
  <!-- End Header -->

  <!-- Begin Content Container -->
  
</body>

</html>
