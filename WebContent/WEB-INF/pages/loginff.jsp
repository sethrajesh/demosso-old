<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>EResponse SSO Login Page</title>
    <!--  for IE8 support -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <link href="css/login.css" rel="stylesheet" type="text/css" media="all"/>
    <!--[if lt IE 7]>
    <script type="text/javascript" src="js/minwidthfix_IE6.js">
   </script>
    <![endif]-->
<style type="text/css">
.bgimg {
    background-image: url(images/middle1.png);
}
.pictureInput {
      float:right
      color: #ffffff;
      margin:10px 0px;
      height:19px;
      width:148px;
      border: 0px solid;
    }
    
.pictureInputP {
      float:right
      color: #ffffff;
      margin:2px 0px;
      height:19px;
      width:148px;
      border: 0px solid;
    }
 
.button
            {
            background: url(images/btn.png) no-repeat;
            cursor:pointer;
            border: none;
            }
 
 input[type=submit] {
    background: url(images/btn.png) no-repeat;
    border: 0;
    display: block;
    height: 41px;
    width: 85px;
}
 
    
</style>

</head>

<body bgcolor="#ffffff">


 <script type="text/javascript">
	var userAg = navigator.userAgent;
	if (userAg.indexOf("Chrome") != -1) {	
        document.location="./nosupport.jsp";

 	}

        if (userAg.indexOf("Firefox") != -1) {
        document.location="./nosupport.jsp";

        }
</script>
  <div align="center">
    <div>
      <img src="images/top.png" alt="EResponse Logo"/>
    </div>
    <div align="center" class="bgimg" style="height: 480px;width: 1000px">
      <div>
  		<br/>
		<p>&nbsp;</p>
      	<p style="width: 504px; text-align: center;"></p>
      	<p style="width: 504px; text-align: center;"><h5>&nbsp;</h5></p>
          <div class="s_formrow">
           	<c:if test="${not empty error}">
		<div class="errorblock">
			Your login attempt was not successful, try again.<br /> Caused :
			${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
		</div>
	</c:if>

	<form name='f' action="<c:url value='j_spring_security_check' />" method="POST">

		<table>
		    <tr>
				<td>
				</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;<input type='text' name='j_username' value="demo" class="pictureInput">
				</td>
			</tr>
			<tr>
					<td><input type='password' name='j_password' value="demo" class="pictureInputP"/>
				</td>
			</tr>
			<tr>
					<td>&nbsp;
				</td>
			</tr>
			<tr>
					<td>&nbsp;
				</td>
			</tr>
			<tr>
					<td>&nbsp;
				</td>
			</tr>

			<tr>
				<td colspan='2'>
				<input class="picbtn" name="button" type="submit" value=""/>
				<div class="divider"/>		
				</td>
			</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan='2'>
				</td>
			</tr>
		</table>

	</form>
    </div>
  </div>
  <!-- End Header -->

  <!-- Begin Content Container -->
  
</body>

</html>
