<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>

    <link href="css/default.css" rel="stylesheet" type="text/css" media="all"/>

  <title></title>
</head>


<body>

<div id="pagecontainer">

  <!-- Begin Header -->
  <div id="headercontainer">
    <div id="logobox">
      <img src="images/SIDES_EResponse_final.png" width="172" height="49" alt="EResponse Logo"/>
    </div>
    <div id="SIDESlogobox">
      <img src="images/SIDES_MerchLogo_White.png" width="172" height="49" alt="SIDES Logo"/>
    </div>
  </div>
  <!-- End Header -->

  <!-- Begin Content Container -->
  <div id="maincontainer">
    <div id="leftbar">
        <p>&nbsp;</p>
    </div><!-- leftbar -->
    <div id="maincontent">
      <div class="s_formfieldsset">
  		<br/>
		<p>&nbsp;</p>
      	<p style="width: 504px; text-align: center;">Login to Sides SSO Test Module</p>
        <form>
      	<p style="width: 504px; text-align: center;"><B>Notice of UI Claim Filing</B></p>
          <div class="s_formrow">
           	<c:if test="${not empty error}">
		<div class="errorblock">
			Your login attempt was not successful, try again.<br /> Caused :
			${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
		</div>
	</c:if>

	<form name='f' action="<c:url value='j_spring_security_check' />" method="POST">

		<table>
			<tr>
				<td>User:</td>
				<td><input type='text' name='j_username' value=''>
				</td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type='password' name='j_password' />
				</td>
			</tr>
			<tr>
				<td colspan='2'>
				<table border="0"> 
				<tr>
				<td><input name="submit" type="submit" value="submit" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>


				</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan='2'>
				</td>
			</tr>
		</table>

	</form>
           
           
           
           </div> <!--s_formrow -->
          
          <br/>
      </div><!-- formfieldsset -->
    </div><!-- maincontent -->
  </div>
  <!-- End Content Container -->

  <!-- Begin Footer -->
  <div id="footercontainer">
    Copyright &copy; 2008 - 2014, National Association of State Workforce Agencies. All Rights Reserved.
  </div>
  <!-- End Footer -->

</div>
</body>

</html>