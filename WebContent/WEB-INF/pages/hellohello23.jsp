<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>EResponse SSO Login Page</title>
    <!--  for IE8 support -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <link href="css/login.css" rel="stylesheet" type="text/css" media="all"/>
    <!--[if lt IE 7]>
    <script type="text/javascript" src="js/minwidthfix_IE6.js"></script>
    <![endif]-->
<style type="text/css">
.bgimg {
    background-image: url(images/page3.png);
}
input[type=submit] {
    background: url(images/SIDES_EResponse_Page2-2-1.png) no-repeat;
    border: 0;
    display: block;
    height: 24px;
    width: 137px;
    vertical-align: middle;
}

#wrapper {
    width: 1092px;
    border: 0px;
    overflow: hidden; /* add this to contain floated children */
	align:right;
}
#first {
    width: 143px;
    float:right; /* add this */
    border: 0px solid red;
}
#second {
    border: 0px solid green;
    float: right; /* add this */
    margin-left: 2px;
}

</style>

</head>

<body bgcolor="#ffffff">

  <!-- Begin Header -->
  <div align="center">
	<img src="images/page2-1.png"/>
  </div>
  
   <div id="wrapper"> 
    <div id="second">     
       <a id="ssoLink" href="<c:url value="/${username}" />"><img src="images/SIDES_EResponse_Page2-2-2.png" alt="SIDES Logo"/></a>   
    </div>
    <div id="first">     
     <form target="_blank" action="<c:url value="/encrypttocas/${username}" />" method="POST">
     
     <input name="button" type="submit" value="" valign="middle"/><br></br>     
    </form> 
    </div>
    
   </div> 
  
  <div align="center">
	<img src="images/page2-3.png"/>
  </div>

  <!-- End Header -->

  <!-- Begin Content Container -->
  
</body>

</html>