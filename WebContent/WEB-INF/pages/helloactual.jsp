<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<title>State SSO link test</title>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<c:url value='/js/jsencrypt.js'/>"></script>
<script type="text/javascript">
  // Call this code when the page is done loading.
  $(function()
  {

    //run for Sso link test
    $('#testSso')
        .click(
            function()
            {
              
            	//start URL 
			  var redirectUrl = "https://testuisides.org/sides-cas/login?service=https%3A%2F%2Ftestuisides.org%2Fsew-si-cas%2Fj_spring_cas_security_check&secureCredentials=";              
              var credentials = $('#stateCode').val() + "|" + $('#fein').val()
                  + "|" + $('#sein').val() + "|" + $('#pin').val() + "|"
                  + new Date().getTime();
              $('#input').val(credentials);
              // Encrypt with the public key...
              var encrypt = new JSEncrypt();
              encrypt.setPublicKey($('#pubkey').val());
              var encrypted = encrypt.encrypt($('#input').val());
              $('#encryptedText').val(encrypted);
              $('#ssoLink').attr("href", redirectUrl + encrypted);
            });

  });
</script>
</head>
<body>
	<form target="_blank" action="<c:url value="/encrypttocas/${username}" />" method="POST">

	<input type="hidden" id="pubkey"
		value="-----BEGIN PUBLIC KEY-----
	MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp2DgJQH0vOeRYNYYh9gX
	rtoEyQPLVG0mEanex8DtqG3oKeUKaJwXSKlEEVBg6lFdMNFslHu1i/KLNCfy0rAr
	hyPbc77LsXSc1ox6JbJw5eiOAjEbMMu7s0nEw4yei+KkyvOYTokhltU5JfPhkxmn
	2tnACL0IqhByvVONSY3SnQdPhaKfafHEjCGMXM3ZxviYCP21EFlf27GOr146Oymb
	h0UM4BRgQaAJfRnUXEVLl+mFrnd1uZMI3A9wBiz0I/9XqnFdeY14cKAyVRR8+3tX
	j9QxUWgeTZgimhsjXw98g8KtkWt0EkcobZRenIOaTfNeyDSFVFum6pyDtFVSsegq
	1QIDAQAB -----END PUBLIC KEY-----" />

	<label for="stateCode">State Code:</label>
	<input type="text" id="stateCode" value="${stateCode}" />
	<br />
	<br />
	<label for="fein">FEIN:</label>
	<input type="text" id="fein" value="${fein}" />
	<br />
	<label for="sein">SEIN:</label>
	<input type="text" id="sein" value="${sein}" />
	<br />
	<label for="pin">PIN:</label>
	<input type="text" id="pin" value="${pin}" />
	<br />

	<input id="testSso" type="button" value="Generate SSO link below" />
	<br />
	<br />
	<a id="ssoLink" href="#" target="_blank">SSO login to SEW-SI Using Javascript</a>
	&nbsp;&nbsp;&nbsp;&nbsp;
	
	<input type="submit" value="Server Side SSO Login to Sides "/>

              
	<br />
	<br />
	<label for="input">Cleartext credentials parameter:</label>
	<br />
	<textarea id="input" name="input" type="text" rows=4 cols=70>cleartext will appear here</textarea>
	<br />
	<label for="input">Encrypted credentials parameter:</label>
	<br />
	<textarea id="encryptedText" name="encryptedText" type="text" rows=4
		cols=70>encrypted text will appear here</textarea>
	<br />
	
		
    </form>
	
	
</body>
</html>