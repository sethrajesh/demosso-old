<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>EResponse SSO Login Page</title>
    <!--  for IE8 support -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <link href="css/default.css" rel="stylesheet" type="text/css" media="all"/>
    <!--[if lt IE 7]>
    <script type="text/javascript" src="js/minwidthfix_IE6.js"></script>
    <![endif]-->
</head>


<body>

<div id="pagecontainer">

  <!-- Begin Header -->
  <div id="headercontainer">
    <div id="logobox">
      <img src="images/top.png" width="869" height="49" alt="EResponse Logo"/>
    </div>
  </div>
  <!-- End Header -->

  <!-- Begin Content Container -->
  <div id="maincontainer">
    <div id="leftbar">
        <p>&nbsp;</p>
    </div><!-- leftbar -->
    <div id="maincontent">
      <div class="s_formfieldsset">
  		<br/>
		<p>&nbsp;</p>
      	<p style="width: 504px; text-align: center;"></p>
      	<p style="width: 504px; text-align: center;"><h5>Sides SSO Test Tool</h5></p>
          <div class="s_formrow">
           	<c:if test="${not empty error}">
		<div class="errorblock">
			Your login attempt was not successful, try again.<br /> Caused :
			${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
		</div>
	</c:if>

	<form name='f' action="<c:url value='j_spring_security_check' />" method="POST">

		<table >
			<tr>
				<td><b>User:</b></td>
				<td><input type='text' name='j_username' value=''>
				</td>
			</tr>
			<tr>
				<td><b>Password:</b></td>
				<td><input type='password' name='j_password' />
				</td>
			</tr>
			<tr>
				<td colspan='2'>
				<input name="submit" type="submit" value="Login"/>
				<div class="divider"/>		
				</td>


				</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan='2'>
				</td>
			</tr>
		</table>

	</form>
           
           
           
           </div> <!--s_formrow -->
          
          <br/>
      </div><!-- formfieldsset -->
    </div><!-- maincontent -->
  </div>
  <!-- End Content Container -->

  <!-- Begin Footer -->
  <div id="footercontainer">
    Copyright &copy; 2008 - 2014, National Association of State Workforce Agencies. All Rights Reserved.
  </div>
  <!-- End Footer -->

</div>
</body>

</html>