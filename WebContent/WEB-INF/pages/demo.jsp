<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>EResponse SSO Login Options</title>
    <!--  for IE8 support -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <link href="css/default.css" rel="stylesheet" type="text/css" media="all"/>
    <!--[if lt IE 7]>
    <script type="text/javascript" src="js/minwidthfix_IE6.js"></script>
    <![endif]-->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<c:url value='/js/jsencrypt.js'/>"></script>    
<script type="text/javascript">
  // Call this code when the page is done loading.
  $(function()
  {

    //run for Sso link test
    $('#testSso')
        .click(
            function()
            {
              
            	//start URL 
			  var redirectUrl = "https://testuisides.org/sides-cas/login?service=https%3A%2F%2Ftestuisides.org%2Fsew-si-cas%2Fj_spring_cas_security_check&secureCredentials=";              
              var credentials = $('#stateCode').val() + "|" + $('#fein').val()
                  + "|" + $('#sein').val() + "|" + $('#pin').val() + "|"
                  + new Date().getTime();
              $('#input').val(credentials);
              // Encrypt with the public key...
              var encrypt = new JSEncrypt();
              encrypt.setPublicKey($('#pubkey').val());
              var encrypted = encrypt.encrypt($('#input').val());
              $('#encryptedText').val(encrypted);
              $('#ssoLink').attr("href", redirectUrl + encrypted);
            });

  });
</script>   
</head>


<body>

<div id="pagecontainer">

  <!-- Begin Header -->
  <div id="headercontainer">
    <div id="logobox">
      <img src="images/SIDES_EResponse_final.png" width="172" height="49" alt="EResponse Logo"/>
    </div>
    <div id="statelogobox">
			<img src="images/state-vi.jpg" width="525" height="49" alt="STATE Logo and Link to Home Page"/>
	</div>
    <div id="SIDESlogobox">
      <img src="images/SIDES_MerchLogo_White.png" width="172" height="49" alt="SIDES Logo"/>
    </div>
  </div>
  <!-- End Header -->

  <!-- Begin Content Container -->
  <div id="maincontainer">
    <div id="leftbar">
        <p>&nbsp;</p>
    </div><!-- leftbar -->
    <div id="maincontent">
      <div class="s_formfieldsset">
  		<br/>
		<p>&nbsp;</p>
      	<p style="width: 504px; text-align: center;"></p>
      	<p style="width: 504px; text-align: center;font-size=20px;"><h5>SSO Parameters/SEW-SI Login Options</h1></p>
        	<form target="_blank" action="<c:url value="/encrypttocas/${username}" />" method="GET">

	<input type="hidden" id="pubkey"
		value="-----BEGIN PUBLIC KEY-----
	MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp2DgJQH0vOeRYNYYh9gX
	rtoEyQPLVG0mEanex8DtqG3oKeUKaJwXSKlEEVBg6lFdMNFslHu1i/KLNCfy0rAr
	hyPbc77LsXSc1ox6JbJw5eiOAjEbMMu7s0nEw4yei+KkyvOYTokhltU5JfPhkxmn
	2tnACL0IqhByvVONSY3SnQdPhaKfafHEjCGMXM3ZxviYCP21EFlf27GOr146Oymb
	h0UM4BRgQaAJfRnUXEVLl+mFrnd1uZMI3A9wBiz0I/9XqnFdeY14cKAyVRR8+3tX
	j9QxUWgeTZgimhsjXw98g8KtkWt0EkcobZRenIOaTfNeyDSFVFum6pyDtFVSsegq
	1QIDAQAB -----END PUBLIC KEY-----" />

	<label for="stateCode">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;State Code:</label>
	<input type="text" id="stateCode" value="${stateCode}" />
	<br /><br />
	<label for="fein">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FEIN:</label>
	<input type="text" id="fein" value="${fein}" />
	<br /><br />
	<label for="sein">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SEIN:</label>
	<input type="text" id="sein" value="${sein}" />
	<br /><br />
	<label for="pin">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PIN:</label>
	<input type="text" id="pin" value="${pin}" />
	<br /><br /><br />
    
   <div>
	<label for="input">dedededed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input id="testSso" type="button" value="Generate SSO link below" />
	</div>
	<br />
	<br />

	<label for="input">&nbsp;&nbsp;&nbsp;Cleartext credentials parameter:</label>
	<br />
	<textarea id="input" name="input" type="text" rows=4 cols=70>cleartext will appear here</textarea>
	<br /><br /><br /><br />
	&nbsp;<label for="input">Encrypted credentials parameter:</label>
	<br />
	
	<textarea id="encryptedText" name="encryptedText" type="text" rows=4
		cols=70>encrypted text will appear here</textarea>
	
	<br />
	<br />	
	<br></br>
<br></br>

	
<a id="ssoLink" href="#" target="_blank">Option 1 - SSO login to SEW-SI Using Javascript</a>
<br></br>
<br></br>

	<input type="submit" value="Option 2 - SSO login using server side"/>
		
		
		
    </form>  
          
          
          
          
           
           
           </div> <!--s_formrow -->
          
          <br/>
      </div><!-- formfieldsset -->
    </div><!-- maincontent -->
  </div>
  <!-- End Content Container -->

  <!-- Begin Footer -->
  <div id="footercontainer">
    Copyright &copy; 2008 - 2014, National Association of State Workforce Agencies. All Rights Reserved.
  </div>
  <!-- End Footer -->

</div>
</body>

</html>
