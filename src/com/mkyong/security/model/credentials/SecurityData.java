package com.mkyong.security.model.credentials;

public class SecurityData
{
  String state;
  String fein;
  String pin;
  String sein;
  
  public String getState()
  {
    return this.state;
  }
  
  public void setState(String state)
  {
    this.state = state;
  }
  
  public String getFein()
  {
    return this.fein;
  }
  
  public void setFein(String fein)
  {
    this.fein = fein;
  }
  
  public String getPin()
  {
    return this.pin;
  }
  
  public void setPin(String pin)
  {
    this.pin = pin;
  }
  
  public String getSein()
  {
    return this.sein;
  }
  
  public void setSein(String sein)
  {
    this.sein = sein;
  }
}
