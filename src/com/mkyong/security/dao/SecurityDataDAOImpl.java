package com.mkyong.security.dao;

import com.mkyong.security.model.credentials.SecurityData;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;

public class SecurityDataDAOImpl
  implements SecurityDataDAO
{
  @Autowired
  DataSource dataSource;
  
  public SecurityData findByData(String fein)
  {
    String sql = "SELECT * FROM usrs WHERE fein = ?";
    
    Connection conn = null;
    try
    {
      conn = this.dataSource.getConnection();
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setString(1, fein);
      
      SecurityData customer = null;
      ResultSet rs = ps.executeQuery();
      if (rs.next())
      {
        customer = new SecurityData();
        customer.setState(rs.getString("STATE"));
        customer.setFein(rs.getString("FEIN"));
        customer.setSein(rs.getString("SEIN"));
        customer.setPin(rs.getString("PIN"));
      }
      rs.close();
      ps.close();
      return customer;
    }
    catch (SQLException e)
    {
      throw new RuntimeException(e);
    }
    finally
    {
      if (conn != null) {
        try
        {
          conn.close();
        }
        catch (SQLException localSQLException2) {}
      }
    }
  }
  
  public SecurityData findByUserId(String userid)
  {
    String sql = "SELECT * FROM usrs WHERE USERNAME = ?";
    
    Connection conn = null;
    try
    {
      conn = this.dataSource.getConnection();
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setString(1, userid);
      SecurityData customer = null;
      ResultSet rs = ps.executeQuery();
      if (rs.next())
      {
        customer = new SecurityData();
        customer.setState(rs.getString("STATE"));
        customer.setFein(rs.getString("FEIN"));
        customer.setSein(rs.getString("SEIN"));
        customer.setPin(rs.getString("PIN"));
      }
      rs.close();
      ps.close();
      return customer;
    }
    catch (SQLException e)
    {
      throw new RuntimeException(e);
    }
    finally
    {
      if (conn != null) {
        try
        {
          conn.close();
        }
        catch (SQLException localSQLException2) {}
      }
    }
  }
}
