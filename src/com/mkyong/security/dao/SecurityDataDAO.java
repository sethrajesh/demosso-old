package com.mkyong.security.dao;

import com.mkyong.security.model.credentials.SecurityData;

public abstract interface SecurityDataDAO
{
  public abstract SecurityData findByUserId(String paramString);
  
  public abstract SecurityData findByData(String paramString);
}
