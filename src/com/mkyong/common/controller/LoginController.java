package com.mkyong.common.controller;

import java.util.Date;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mkyong.security.dao.SecurityDataDAO;
import com.mkyong.security.model.credentials.SecurityData;
import com.sides.security.exception.SIDESSSOException;
import com.sides.security.util.EncryptUtil;

@Controller
public class LoginController
{
  @Autowired
  SecurityDataDAO securityDataDAO;

  
  @RequestMapping(value={"/welcome"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String printWelcome(ModelMap model) throws SIDESSSOException
  {
    User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    String name = user.getUsername();
    
    model.addAttribute("username", name);
    model.addAttribute("message", "Spring Security login + database example");
    
    SecurityData secData = this.securityDataDAO.findByUserId(name);
    model.addAttribute("secdata", secData);
    
    model.addAttribute("stateCode", secData.getState());
    model.addAttribute("fein", secData.getFein());
    model.addAttribute("sein", secData.getSein());
    model.addAttribute("pin", secData.getPin());
    
    String params = secData.getState().trim() + "|" + secData.getFein() + "|" + secData.getSein() + "|" + secData.getPin() + "|" + new Date().getTime();
    
    EncryptUtil encryptUtil = new EncryptUtil();
    
    String encryptUrl = encryptUtil.getEncryptedUrl("ST","123443212","123443211","1234","true",new Boolean("true"),Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,"http://www.google.com","http://www.amazon.com");

    //String encryptUrl = encryptUtil.getEncryptedUrl(params);
    
    model.addAttribute("params", params);
    model.addAttribute("ecryptParams", encryptUrl);
    
    String baseUrl = "https://testuisides.org/sides-cas/login?service=https%3A%2F%2Ftestuisides.org%2Fsew-si-cas%2Fj_spring_cas_security_check&secureCredentials=";
    model.addAttribute("url", encryptUrl);
    return "hello";
  }
  
  @RequestMapping(value={"/{id}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String printDemo(ModelMap model, @PathVariable String id) throws SIDESSSOException
  {
    System.out.println(" I ID" + id);
    SecurityData secData = this.securityDataDAO.findByUserId(id);
    
    model.addAttribute("username", id);
    model.addAttribute("message", "Spring Security login + database example");
    
    model.addAttribute("secdata", secData);
    
    model.addAttribute("stateCode", secData.getState());
    model.addAttribute("fein", secData.getFein());
    model.addAttribute("sein", secData.getSein());
    model.addAttribute("pin", secData.getPin());
    
    String params = secData.getState().trim() + "|" + secData.getFein() + "|" + secData.getSein() + "|" + secData.getPin() + "|" + new Date().getTime();
    
    EncryptUtil encryptUtil = new EncryptUtil();
    
    String encryptUrl = encryptUtil.getEncryptedUrl("ST","123443212","123443211","1234","true",new Boolean("true"),Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,"http://www.google.com","http://www.amazon.com");
    System.out.println("Url 1:" + encryptUrl );
    
    model.addAttribute("params", params);
    model.addAttribute("ecryptParams", encryptUrl);
    
    String baseUrl = "https://testuisides.org/sides-cas/login?service=https%3A%2F%2Ftestuisides.org%2Fsew-si-cas%2Fj_spring_cas_security_check&secureCredentials=";
    model.addAttribute("url", encryptUrl);
    return "demo";
  }
  
  @RequestMapping(value={"/encrypttocas/{id}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String getEncryptedUrl(ModelMap model, @PathVariable String id) throws Exception
  {
    SecurityData secData = this.securityDataDAO.findByUserId(id);
    String params = secData.getState().trim() + "|" + secData.getFein() + "|" + secData.getSein() + "|" + secData.getPin() + "|" + new Date().getTime();
    EncryptUtil encryptUtil = new EncryptUtil();
    
    String encryptUrl = encryptUtil.getEncryptedUrl("ST|123443212|123443211|1234","true");
    
    //String baseUrl = "https://testuisides.org/sides-cas/login?service=https%3A%2F%2Ftestuisides.org%2Fsew-si-cas%2Fj_spring_cas_security_check&secureCredentials=";
    
    return "redirect:" + encryptUrl;
  }

  @RequestMapping(value={"/encrypttocas/new/{id}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String getEncryptedUrll(ModelMap model, @PathVariable String id) throws SIDESSSOException
  {
    SecurityData secData = this.securityDataDAO.findByUserId(id);
    String params = secData.getState().trim() + "|" + secData.getFein() + "|" + secData.getSein() + "|" + secData.getPin() + "|" + new Date().getTime();
    EncryptUtil encryptUtil = new EncryptUtil();
    
    String encryptUrl = encryptUtil.getEncryptedUrl("ST","123443212","123443211","1234","true",new Boolean("true"),Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,"http://www.google.com","http://www.amazon.com");

    
    //String baseUrl = "https://testuisides.org/sides-cas/login?service=https%3A%2F%2Ftestuisides.org%2Fsew-si-cas%2Fj_spring_cas_security_check&secureCredentials=";
    

    return "redirect:" + encryptUrl;
  }

  @RequestMapping(value={"/encrypttocaas/{id}"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String getEncryptedUrls(ModelMap model, @PathVariable String id) throws SIDESSSOException
  {
    SecurityData secData = this.securityDataDAO.findByUserId(id);
    String params = secData.getState().trim() + "|" + secData.getFein() + "|" + secData.getSein() + "|" + secData.getPin() + "|" + new Date().getTime();
    EncryptUtil encryptUtil = new EncryptUtil();
    
    String encryptUrl = encryptUtil.getEncryptedUrl("ST","123443212","123443211","1234","true",new Boolean("true"),Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE,"http://www.google.com","http://www.amazon.com");

    System.out.println("Parameters :" + params);
    
    String baseUrl = "https://testuisides.org/sides-cas/login?service=https%3A%2F%2Ftestuisides.org%2Fsew-si-cas%2Fj_spring_cas_security_check&secureCredentials=";
    
    System.out.println("Url 3:" + encryptUrl );
    
    return "redirect:" + encryptUrl;
  }
  
  @RequestMapping(value={"/login"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String login(ModelMap model)
  {
    return "login";
  }
  
  @RequestMapping(value={"/loginfailed"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String loginerror(ModelMap model)
  {
    model.addAttribute("error", "true");
    return "login";
  }
  
  @RequestMapping(value={"/logout"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String logout(ModelMap model)
  {
    return "login";
  }
}
