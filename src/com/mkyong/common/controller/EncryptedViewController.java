package com.mkyong.common.controller;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.URLEncoder;
import java.security.Key;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import org.springframework.security.core.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class EncryptedViewController
{
  @RequestMapping(value={"/ecrypt/{id}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String getEncryptedUrl(ModelMap model, @PathVariable String id)
  {
    Map<String, String> map = new HashMap();
    map.put("id", id);
    map.put("topSecret", "Waffles are tasty");
    try
    {
      String[] encrypted = encryptObject(map);
      
      String urlVal = "http://localhost:8080/view?d=" + encrypted[0] + "&v=" + encrypted[1];
      model.addAttribute("username", urlVal);
      model.addAttribute("message", urlVal);
    }
    catch (Exception localException) {}
    return "hello";
  }
  
  private String[] encryptObject(Object obj)
    throws Exception
  {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    ObjectOutput out = new ObjectOutputStream(stream);
    FileInputStream fis = new FileInputStream("/uis/app/WebSphereV8/AppServer/profiles/AppSrv01/installedApps/localhostNode01Cell/sides-broker-si_war.ear/sides-broker-si.war/WEB-INF/classes/broker.jks");
    try
    {
      KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
      
      char[] password = "4stat3sandtp2s".toCharArray();
      char[] keypassword = "keypass".toCharArray();
      ks.load(fis, password);
      
      Certificate cert = ks.getCertificate("broker");
      Key myKey = cert.getPublicKey();
      
      out.writeObject(obj);
      byte[] serialized = stream.toByteArray();
      
      Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
      byte[] iv = new byte[cipher.getBlockSize()];
      new SecureRandom().nextBytes(iv);
      IvParameterSpec ivSpec = new IvParameterSpec(iv);
      cipher.init(1, myKey);
      
      byte[] encrypted = cipher.doFinal(serialized);
      byte[] base64Encoded = Base64.encode(encrypted);
      String base64String = new String(base64Encoded);
      String urlEncodedData = URLEncoder.encode(base64String, "UTF-8");
      
      byte[] base64IV = Base64.encode(iv);
      String base64IVString = new String(base64IV);
      String urlEncodedIV = URLEncoder.encode(base64IVString, "UTF-8");
      
      return new String[] { urlEncodedData, urlEncodedIV };
    }
    catch (Exception e)
    {
      e.printStackTrace();
      System.out.println("-----------------" + e);
    }
    finally
    {
      stream.close();
      fis.close();
      out.close();
    }
    return new String[] { "a" };
  }
}
