package com.mkyong.common.controller;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class CustomUserDetails
  implements UserDetails
{
  private String company;
  
  public CustomUserDetails(String company)
  {
    this.company = company;
  }
  
  public String getCompany()
  {
    return this.company;
  }
  
  public void setCompany(String company)
  {
    this.company = company;
  }
  
  public Collection<GrantedAuthority> getAuthorities()
  {
    return null;
  }
  
  public String getPassword()
  {
    return null;
  }
  
  public String getUsername()
  {
    return null;
  }
  
  public boolean isAccountNonExpired()
  {
    return false;
  }
  
  public boolean isAccountNonLocked()
  {
    return false;
  }
  
  public boolean isCredentialsNonExpired()
  {
    return false;
  }
  
  public boolean isEnabled()
  {
    return false;
  }
}
